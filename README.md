# Vdirsyncer 

My simple configuration [vdirsyncer](https://github.com/pimutils/vdirsyncer) 
(`CalDav` and `CardDav` sync) with [khal](https://github.com/pimutils/khal) (Calendar), 
[khard](https://github.com/scheibler/khard) (Contacts)
and [Todoman](https://github.com/pimutils/todoman) (tasks manager)

![Print screen](https://pixelfed.social/storage/m/113a3e2124a33b1f5511e531953f5ee48456e0c7/85c26953a179a0e533565ce79e82deb9724813d5/SiWB2RaPt4oiqDWp1KwR6ddrLGwKhatUJi7Xh1GL.png)

Requirements

- [NextCloud Calendar](https://apps.nextcloud.com/apps/calendar)
- [NextCloud Contacts](https://apps.nextcloud.com/apps/contacts)
- [NextCloud Tasks](https://apps.nextcloud.com/apps/tasks)

Install

```
# apt install -y vdirsyncer khal khard todoman
```

```
$ cp -r .config "$HOME"/
```
## Configure

In the examples below uses multiple accounts (personal and work)
remove, replicate and rename, if necessary

### NextCloud Calendar (CaldDav)

Change the uppercase words in the `"$HOME"/.config/vdirsyncer` file,
according to your NextCloud provider

```
type = "caldav"
url = "PROVIDER"
username = "USER"
password = "PASSWORD"
```

Changes for contacts to be synchronized

```
collections = ["personal", "work"]
```

Create local contacts

```
$ vdirsyncer discover contacts
```

Note: Cache location `"$HOME"/.vdirsyncer`

### NextCloud Contacts (CardDav)

Change the uppercase words in the `"$HOME"/.config/vdirsyncer` file,
according to your NextCloud provider

```
type = "carddav"
url = "PROVIDER"
username = "USER"
password = "PASSWORD"
```

Changes for contacts to be synchronized

```
collections = ["personal", "work"]
```

Create local calendars

```
$ vdirsyncer discover calendars 
```

Note: Cache location `"$HOME"/.vdirsyncer`

Change the uppercase words in the `"$HOME"/.config/khard/khard.conf` file,
according to your NextCloud provider

```
[addressbooks]
[[personal]]
path = ~/.contacts/PERSONAL
[[work]]
path = ~/.contacts/WORK
```

Change the uppercase words in the `"$HOME"/.config/khal/conf` file,
according to your NextCloud provider

```
[[personal]]
path = ~/.calendars/PERSONAL/
color = light green 

[[sepbit]]
path = ~/.calendars/WORK/
color = light blue
```

Note: Cache location `"$HOME"/.local/share/khal`

## Basic use

Calendar and contact sync

```
$ vdirsyncer sync
```

### Optional 

Auto sync, execute `$ crontab -e` and add the lines

```
*/15 * * * * /bin/vdirsyncer -v WARNING sync
```
